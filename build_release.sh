#!/bin/bash
cargo build --target wasm32-unknown-unknown --release

cwd=$(pwd)
cd target/wasm32-unknown-unknown/release

wasm-bindgen --target web --no-typescript --out-dir . deca_time_wasm.wasm
wasm-gc deca_time_wasm_bg.wasm

cp deca_time_wasm_bg.wasm ../../../www/
cp deca_time_wasm.js ../../../www/

cd $cwd
