## Deca Time as WebAssembly module

Want to get the current Deca Time in your browser or other environment which supports web assembly? Now you can.

### Installation
#### Setup

Make sure that you have the [Rust toolchain](https://rustup.rs/) installed.

You also need to have the target for web assembly installed.

```sh
rustup target add wasm32-unknown-unknown
```

The following section assumes you're building in linux. But it should not be too difficult to adapt to other environments based on the contents of the bash scripts.

Now run ```setup.sh``` to install the remaining build tools.

If you have run setup previously, you may need to update the ```wasm-bindgen``` tool as well. Cargo will provide details when building if this is the case.

#### Build

In order to build a test version, you can run ```build.sh``` which builds and copies the files to the ```www``` directory.

Enter the directory and run ```python server.py``` to start a simple server to launch the example code. Open http://localhost:8080 in your browser to view the results.

Studying index.html shows how to import the web assembly module and make it available to javascritp in the browser. When it is loaded, the functions will be available to your scripts.

Run ```build_release.sh``` to build in release mode. This will also copy the resulting files to the www folder.

A live version of the example can currently be found [here.](https://djeekay.net/dit/wasm/)
